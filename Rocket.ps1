﻿Function Start-Rocket
{
[CmdletBinding()]
param (
    [Parameter(Mandatory=$true, ValueFromPipeline=$true, Position=0)]
    [string]$Packages,
    [string]$LogPath = 'C:\Windows\Logs\Rocket.log'
)
    Write-Host "######  ######  ######  ##   ##  ######  ######" -ForegroundColor Yellow
    Write-Host "##  ##  ##  ##  ##  ##  ## ##    ##        ##  " -ForegroundColor Yellow
    Write-Host "######  ##  ##  ##      ####     ####      ##  " -ForegroundColor Yellow
    Write-Host "####    ##  ##  ##      ## ##    ##        ##  " -ForegroundColor Yellow
    Write-Host "## ##   ##  ##  ##  ##  ##  ##   ##        ##  " -ForegroundColor Yellow
    Write-Host "##  ##  ######  ######  ##   ##  ######    ##  " -ForegroundColor Yellow

    #Exit if PS version lower than 3
    if ($PSVersionTable.PSVersion.Major -lt 3)
    {
        Write-Host "Powershell v$($PSVersionTable.PSVersion.Major) not supported" -ForegroundColor Yellow
        exit 1
    }
    if (Test-Path $LogPath)
    {
        Remove-Item $LogPath
    }

    $computername = ($env:COMPUTERNAME).ToUpper()
    $timestamp = Get-Date -Format "dd/MM/yyyy HH:mm"
    $starttime = Get-Date
    Add-Content -Path $LogPath -Value "Starting Rocket $timestamp"
    Write-Host "Starting Rocket $timestamp" -ForegroundColor Yellow

    Function Install-AppPackage ($packageName, $version)
    {
        If ([String]::IsNullOrEmpty($version))
        {
           &choco upgrade $packageName -y 
        }
        else
        {
           &choco install $packageName -version $version -y --allow-downgrade
        }   
    }

    if ($Packages -ne $null)
    {
        Try 
        {
            $rocketPackages = ConvertFrom-Json $Packages
        }
        catch [System.ArgumentException]
        {
            Add-Content -Path $LogPath -Value "$($Error[0].Exception.Message)"
            Write-Host $($Error[0].Exception.Message) -ForegroundColor Yellow
            exit 1
        }
    }

    if ($rocketPackages -ne $null)
    {
        $scriptPackages = $rocketPackages | ? {$_.packageprovider -eq 'script'}
        $chocolateyPackages = $rocketPackages | ? {$_.packageprovider -eq 'chocolatey'}
        if (!([string]::IsNullOrEmpty($scriptPackages)))
        {
            foreach ($scriptPackage in $scriptPackages)
            {
                $ScriptCmd = $null
                $ScriptCmd = $scriptPackage.package
                Write-Host "Executing Action: $ScriptCmd" -ForegroundColor Yellow
                Add-Content -Path $LogPath -Value "Delivering Package: $ScriptCmd"
                Invoke-Expression -Command $ScriptCmd
            }
        }
        if (!([string]::IsNullOrEmpty($chocolateyPackages)))
        {
            foreach ($chocolateyPackage in $chocolateyPackages)
            {
                $packageName = $chocolateyPackage.package
                $packageVersion = $chocolateyPackage.version
                Write-Host "Delivering Package: $packageName $packageVersion" -ForegroundColor Yellow
                Add-Content -Path $LogPath -Value "Delivering Package: $packageVersion"
                Install-AppPackage -packageName $packageName -version $packageVersion
            }        
        }
    }
    else
    {
        Write-Host "No actions found" -ForegroundColor Yellow
    }




    $timeSpan = New-TimeSpan $starttime $(Get-Date)
    $hours = $timeSpan.Hours
    if ($hours -le 9)
    {
        $hours = "0" + $hours
    }
    $minutes = $timeSpan.Minutes
    if ($minutes -le 9)
    {
        $minutes = "0" + $minutes
    }
    $seconds = $timeSpan.Seconds
    if ($seconds -le 9)
    {
        $seconds = "0" + $seconds
    }
    [string]$wasrunningfor = "${hours}:${minutes}:${seconds}"
    Add-Content -Path $LogPath -Value "Rocket runtime: $wasrunningfor"
    Write-Host "Rocket runtime: $wasrunningfor" -ForegroundColor Yellow
}