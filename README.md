# Rocket #

Powershell Start-Rocket function. Gets actions from JSON and runs them.

Action providers:

* Script 
* Chocolatey

### JSON example ###


```
#!json


[
 
    {
        "packageprovider" : "chocolatey",
        "package" : "7-zip",
        "version" : ""
    }, 
    {
        "packageprovider" : "script",
        "package" : "Get-Process",
        "version" : ""
    }, 
    {
        "packageprovider" : "chocolatey",
        "package" : "flashplayeractivex",
        "version" : ""
    }
]
```

### Usage ###

* Example 1

```
#!powershell

iex ((new-object net.webclient).DownloadString('https://bitbucket.org/aartemjevas/rocket/raw/master/Rocket.ps1'))
$json = @"

[
 
    {
        "packageprovider" : "chocolatey",
        "package" : "7-zip",
        "version" : ""
    }, 
    {
        "packageprovider" : "chocolatey",
        "package" : "flashplayerplugin",
        "version" : ""
    }, 
    {
        "packageprovider" : "chocolatey",
        "package" : "flashplayeractivex",
        "version" : ""
    }
]
"@
Start-Rocket -Packages $json
```
* Example 2

```
#!powershell

$uri = 'http://internal.webservice.lv/rocket.aspx?computername=LAPTOP1&action=get'
(Invoke-WebRequest -Uri $uri).content | Start-Rocket
```

* Example 3

```
#!powershell

$json = @"
[
    {
        "packageprovider" : "script",
        "package" : "Write-Host \"Hey!\"",
        "version" : ""
    }, 
    {
        "packageprovider" : "chocolatey",
        "package" : "7-zip",
        "version" : ""
    }
]
"@
Start-Rocket -Packages $json
```

![rocketscr.png](https://bitbucket.org/repo/nagbj9/images/3643596633-rocketscr.png)

* Example 4 - logon script


```
#!powershell

#region Load Start-Rocket and Show-PSNotification functions
$uri = 'http://internal.webservice.lv/ps/rocket.ps1',
       'http://internal.webservice.lv/ps/Show-PSNotification.ps1'

$uri | % { iex ((new-object net.webclient).DownloadString($_))}
#endregion

$json = (Invoke-WebRequest -Uri 'http://internal.webservice.lv/rocket.aspx?computername=LAPTOP1&action=get').content
if ($json -ne $null)
{
    $packages = ConvertFrom-Json $json
    $notificationMessage = "I have $($packages.count) new things to deliver!"

    #Show notification
    Show-PSNotification -Size Small -Title "Rocket" -Message $notificationMessage -Timeout 15 -Icon Rocket

    Start-Rocket -Packages $json

    #Delete action list
    Invoke-WebRequest -Uri 'http://internal.webservice.lv/rocket.aspx?computername=LAPTOP1&action=drop'
}

```